package com.mieczkowskidev.textnodes.app

import android.app.Application

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }
}