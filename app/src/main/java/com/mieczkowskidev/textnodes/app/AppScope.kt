package com.mieczkowskidev.textnodes.app

import javax.inject.Scope


/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
@Scope
@Retention
annotation class AppScope