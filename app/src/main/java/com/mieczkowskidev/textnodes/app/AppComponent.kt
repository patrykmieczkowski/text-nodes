package com.mieczkowskidev.textnodes.app

import android.content.Context
import com.google.firebase.database.DatabaseReference
import com.mieczkowskidev.textnodes.textnodes.di.FirebaseModule
import dagger.Component

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
@AppScope
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun getAppContext(): Context
}