package com.mieczkowskidev.textnodes.main.di

import javax.inject.Scope

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
@Scope
@Retention
annotation class MainActivityScope