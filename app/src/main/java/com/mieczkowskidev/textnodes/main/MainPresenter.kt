package com.mieczkowskidev.textnodes.main

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mieczkowskidev.textnodes.textnodes.TextNode
import com.mieczkowskidev.textnodes.textnodes.TextNodesContract
import javax.inject.Inject

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
class MainPresenter @Inject constructor() : MainContract.Presenter {

    companion object {
        private val TAG = MainPresenter::class.java.simpleName
    }

    @Inject
    lateinit var provider: TextNodesContract

    private var view: MainContract.View? = null

    override fun attachView(view: MainContract.View) {
        this.view = view

        startDatabaseListener()
    }

    private fun startDatabaseListener() {
        view?.showProgress()

        val query = provider.getDatabaseQuery()

        query.addValueEventListener(textNodeListener())
    }

    private fun startDatabaseSearchListener(query: String) {
        val queryQ = provider.getDatabaseQuery(query)

        queryQ.addValueEventListener(textNodeListener())
    }

    private fun textNodeListener() = object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            handleDataSnapshot(dataSnapshot)
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.e(TAG, "database error: " + databaseError.message)
        }
    }

    private fun handleDataSnapshot(dataSnapshot: DataSnapshot) {
        val list: ArrayList<TextNode> = ArrayList()

        for (noteSnapshot in dataSnapshot.children) {
            val ns = noteSnapshot.getValue<TextNode>(TextNode::class.java)
            if (ns != null) {
                list.add(ns)
            }
        }

        refreshTextNodesList(list)
    }

    override fun filterList(query: String) {
        Log.d(TAG, "filter list with query: $query")

        startDatabaseSearchListener(query)
    }

    override fun addTextNode(textNode: TextNode) {
        provider.writeTextNode(textNode)
    }

    private fun refreshTextNodesList(textNotes: List<TextNode>) {
        view?.hideProgress()
        if (textNotes.isNotEmpty()) {
            view?.refreshRecycler(textNotes.asReversed())
        } else {
            view?.showNoContent()
        }
    }

    override fun detachView() {
        this.view = null
    }

    override fun removeNode(id: String) {
        provider.removeTextNode(id)
    }

}