package com.mieczkowskidev.textnodes.main.di

import com.google.firebase.database.DatabaseReference
import com.mieczkowskidev.textnodes.app.AppComponent
import com.mieczkowskidev.textnodes.main.MainActivity
import com.mieczkowskidev.textnodes.textnodes.di.FirebaseModule
import dagger.Component

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
@MainActivityScope
@Component(modules = [(MainActivityModule::class), (FirebaseModule::class)], dependencies = [(AppComponent::class)])
interface MainActivityComponent {

    fun injectMainActivity(mainActivity: MainActivity)

    fun getFirebaseDatabase(): DatabaseReference

}