package com.mieczkowskidev.textnodes.main

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.jakewharton.rxbinding2.widget.textChanges
import com.mieczkowskidev.textnodes.R
import com.mieczkowskidev.textnodes.adapter.TextNodesAdapter
import com.mieczkowskidev.textnodes.app.App
import com.mieczkowskidev.textnodes.dialog.TextNodeDialog
import com.mieczkowskidev.textnodes.main.di.DaggerMainActivityComponent
import com.mieczkowskidev.textnodes.main.di.MainActivityModule
import com.mieczkowskidev.textnodes.textnodes.TextNode
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View,
        TextNodesAdapter.TextNodesAdapterListener, TextNodeDialog.TextNodeDialogListener {

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var adapter: TextNodesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initInjection()
        initPresenter()
        initRecycler()
        initButton()
        initSearch()
    }

    private fun initButton() {

        main_add_button.setOnClickListener { newNodeDialog() }
    }

    private fun initInjection() {

        DaggerMainActivityComponent.builder()
                .mainActivityModule(MainActivityModule(this))
                .appComponent((application as App).appComponent)
                .build()
                .injectMainActivity(this)
    }

    private fun initPresenter() {
        presenter.attachView(this)
    }

    private fun initRecycler() {
        adapter.setTextNodesAdapterListener(this)
        main_recycler.layoutManager = LinearLayoutManager(this)
        main_recycler.adapter = adapter
    }

    private fun initSearch() {
        main_search.setHintTextColor(ContextCompat.getColor(this, R.color.foxGrey))

        main_search
                .textChanges()
                .skipInitialValue()
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(
                        { presenter.filterList(it.toString()) },
                        { Log.e(MainActivity::class.java.simpleName, "error - $it") }
                )
    }

    private fun newNodeDialog() = showTextNodeDialog(TextNodeDialog.newInstance())

    private fun editNodeDialog(textNode: TextNode) = showTextNodeDialog(TextNodeDialog.newInstance(textNode))

    private fun showTextNodeDialog(dialog: TextNodeDialog) {
        dialog.show(fragmentManager, TextNodeDialog::class.java.simpleName)
        dialog.textNodeDialogListener = this
    }

    override fun acceptNode(textNode: TextNode) {
        presenter.addTextNode(textNode)
    }

    override fun refreshRecycler(textNodeList: List<TextNode>) {
        adapter.refreshTextNodesList(textNodeList)
    }

    override fun editNode(textNode: TextNode) {
        editNodeDialog(textNode)
    }

    override fun deleteNode(id: String) {
        presenter.removeNode(id)
    }

    override fun showProgress() {
        manageDownloadViews(View.VISIBLE, View.GONE, View.GONE)
    }

    override fun hideProgress() {
        manageDownloadViews(View.GONE, View.VISIBLE, View.GONE)
    }

    override fun showNoContent() {
        manageDownloadViews(View.GONE, View.GONE, View.VISIBLE)
    }

    private fun manageDownloadViews(progressBar: Int, recyclerView: Int, noContent: Int) {
        main_progress.visibility = progressBar
        main_recycler.visibility = recyclerView
        main_no_content.visibility = noContent
    }

    override fun onDestroy() {
        presenter.detachView()

        super.onDestroy()
    }
}
