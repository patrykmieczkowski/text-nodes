package com.mieczkowskidev.textnodes.main.di

import com.mieczkowskidev.textnodes.main.MainActivity
import com.mieczkowskidev.textnodes.main.MainContract
import com.mieczkowskidev.textnodes.main.MainPresenter
import com.mieczkowskidev.textnodes.textnodes.TextNodesContract
import com.mieczkowskidev.textnodes.textnodes.TextNodesProvider
import dagger.Module
import dagger.Provides

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
@Module
class MainActivityModule(private val mainActivity: MainActivity) {

    @Provides
    @MainActivityScope
    fun provideMainActivity(): MainActivity = mainActivity

    @Provides
    @MainActivityScope
    fun provideMainPresenter(mainPresenter: MainPresenter): MainContract.Presenter = mainPresenter

    @Provides
    @MainActivityScope
    fun provideTextNodesProvider(textNodesProvider: TextNodesProvider): TextNodesContract = textNodesProvider
}