package com.mieczkowskidev.textnodes.main

import com.mieczkowskidev.textnodes.textnodes.TextNode

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
interface MainContract {

    interface View {
        fun refreshRecycler(textNodeList: List<TextNode>)
        fun showProgress()
        fun hideProgress()
        fun showNoContent()
    }

    interface Presenter {

        fun attachView(view: View)
        fun detachView()
        fun removeNode(id: String)
        fun addTextNode(textNode: TextNode)
        fun filterList(query: String)
    }
}