package com.mieczkowskidev.textnodes.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.mieczkowskidev.textnodes.R
import com.mieczkowskidev.textnodes.inflate
import com.mieczkowskidev.textnodes.textnodes.TextNode
import kotlinx.android.synthetic.main.text_node_item.view.*
import javax.inject.Inject

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
class TextNodesAdapter @Inject constructor() : RecyclerView.Adapter<TextNodesAdapter.TextNodesViewHolder>() {

    private var textNodesList: List<TextNode> = ArrayList()

    private lateinit var textNodesAdapterListener: TextNodesAdapterListener

    fun refreshTextNodesList(textNodesList: List<TextNode>) {
        this.textNodesList = textNodesList
        notifyDataSetChanged()
    }

    fun setTextNodesAdapterListener(textNodesAdapterListener: TextNodesAdapterListener) {
        this.textNodesAdapterListener = textNodesAdapterListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextNodesViewHolder {
        val itemView = parent.inflate(R.layout.text_node_item)
        return TextNodesViewHolder(itemView, textNodesAdapterListener)
    }

    override fun getItemCount(): Int = textNodesList.size

    override fun onBindViewHolder(holder: TextNodesViewHolder, position: Int) {
        val textNode = textNodesList[position]
        holder.bindViews(textNode)
    }

    class TextNodesViewHolder(itemView: View, private var textNodesAdapterListener: TextNodesAdapterListener) : RecyclerView.ViewHolder(itemView) {

        private var view: View = itemView

        private var textNode: TextNode? = null

        fun bindViews(textNode: TextNode) {
            this.textNode = textNode

            // BODY
            view.adapter_text_node_body.text = textNode.body

            // TAGS
            view.adapter_text_node_tags.text = textNode.tags

            // EDIT
            view.adapter_text_node_edit.setOnClickListener { textNodesAdapterListener.editNode(textNode) }
        }
    }

    interface TextNodesAdapterListener {
        fun editNode(textNode: TextNode)
    }
}