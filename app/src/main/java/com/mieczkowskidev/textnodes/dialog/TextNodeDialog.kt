package com.mieczkowskidev.textnodes.dialog

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mieczkowskidev.textnodes.R
import com.mieczkowskidev.textnodes.textnodes.TextNode
import kotlinx.android.synthetic.main.text_node_dialog.view.*


/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
class TextNodeDialog : DialogFragment() {

    companion object {

        private const val KEY = "node"

        fun newInstance(): TextNodeDialog = TextNodeDialog()

        fun newInstance(textNode: TextNode): TextNodeDialog {
            val textNodeDialog = TextNodeDialog()

            val args = Bundle()
            args.putParcelable(KEY, textNode)
            textNodeDialog.arguments = args

            return textNodeDialog
        }

    }

    lateinit var textNodeDialogListener: TextNodeDialogListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.text_node_dialog, container, false)

        val textNode: TextNode? = arguments?.getParcelable(KEY)
        dialog.window.setBackgroundDrawableResource(android.R.color.transparent)

        if (textNode != null) {
            showDeleteButton(view, textNode)
            view.text_node_dialog_body.setText(textNode.body, TextView.BufferType.EDITABLE)
            view.text_node_dialog_tags.setText(textNode.tags, TextView.BufferType.EDITABLE)
        }

        view.text_node_dialog_button.setOnClickListener { onButtonClicked(view, textNode) }

        return view
    }

    private fun showDeleteButton(view: View, textNode: TextNode) {

        view.text_node_dialog_remove.visibility = View.VISIBLE
        view.text_node_dialog_remove.setOnClickListener { onItemRemove(textNode.id) }

    }

    private fun onItemRemove(id: String) {
        textNodeDialogListener.deleteNode(id)

        dialog.dismiss()
    }

    private fun onButtonClicked(view: View, textNode: TextNode?) {
        if (textNode != null) {
            textNodeDialogListener.acceptNode(TextNode(view.text_node_dialog_body.text.toString(),
                    view.text_node_dialog_tags.text.toString(), textNode.timestamp, textNode.id))
        } else {
            textNodeDialogListener.acceptNode(TextNode(view.text_node_dialog_body.text.toString(),
                    view.text_node_dialog_tags.text.toString()))
        }

        dialog.dismiss()
    }

    interface TextNodeDialogListener {
        fun acceptNode(textNode: TextNode)
        fun deleteNode(id: String)
    }
}