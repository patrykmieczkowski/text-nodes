package com.mieczkowskidev.textnodes.textnodes

import com.google.firebase.database.DatabaseReference
import javax.inject.Inject

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
class TextNodesProvider @Inject constructor(private val database: DatabaseReference) : TextNodesContract {

    companion object {
        private const val TABLE_NAME = "nodes"
    }

    override fun writeTextNode(textNode: TextNode) {
        database.child(TABLE_NAME).child(textNode.id).setValue(textNode)
    }

    override fun removeTextNode(id: String) {
        database.child(TABLE_NAME).child(id).removeValue()
    }

    override fun getDatabaseQuery() = database.child(TABLE_NAME).orderByChild("timestamp")

    override fun getDatabaseQuery(query: String) = database.child(TABLE_NAME)
            .orderByChild("tags")
            .startAt(query)
            .endAt(query + "\uf8ff")

}