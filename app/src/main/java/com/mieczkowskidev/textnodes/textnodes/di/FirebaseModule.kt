package com.mieczkowskidev.textnodes.textnodes.di

import com.google.firebase.database.FirebaseDatabase
import com.mieczkowskidev.textnodes.main.di.MainActivityScope
import dagger.Module
import dagger.Provides

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
@Module
class FirebaseModule {

    @Provides
    @MainActivityScope
    fun provideFirebase() = FirebaseDatabase.getInstance().reference
}