package com.mieczkowskidev.textnodes.textnodes

import android.os.Parcel
import android.os.Parcelable
import java.util.*

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
data class TextNode(var body: String = "", var tags: String = "",
                    var timestamp: Long = System.currentTimeMillis(),
                    var id: String = UUID.randomUUID().toString()) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(body)
        parcel.writeString(tags)
        parcel.writeLong(timestamp)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TextNode> {
        override fun createFromParcel(parcel: Parcel): TextNode {
            return TextNode(parcel)
        }

        override fun newArray(size: Int): Array<TextNode?> {
            return arrayOfNulls(size)
        }
    }
}