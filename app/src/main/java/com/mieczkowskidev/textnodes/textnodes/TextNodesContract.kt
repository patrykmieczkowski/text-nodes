package com.mieczkowskidev.textnodes.textnodes

import com.google.firebase.database.Query

/**
 * Created by Patryk Mieczkowski on 07.06.2018
 */
interface TextNodesContract {

    fun writeTextNode(textNode: TextNode)
    fun removeTextNode(id: String)
    fun getDatabaseQuery(): Query
    fun getDatabaseQuery(query: String): Query
}