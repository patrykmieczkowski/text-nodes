## Text Nodes

Zostały zrealizowane wszystkie założenia dostarczone w dokumencie
1. Aplikacja wyświetla notatki zapisywane w Firebase Realtime Database,
2. Możliwe jest dodawanie nowych, edycja oraz usuwanie,
3. Możliwe jest nadawanie tagów do notatek,
4. Dostępna jest dynamiczna wyszukiwarka przeszukująca po tagach,
5. Layouty zostały przygotowane tak aby w czytelny sposób przedstawić notatki :)

## Uruchomienie aplikacji
1. Minimalne SDK potrzebne do uruchomienia aplikacji to 23
2. W przypadku braku możliwości wciśnięcia przycisku "Run" należy odpalić task "Sync Project With Gradle Files"

## Screeny z aplikacji
<img src="/screen_01.png" width="300">
<img src="/screen_02.png" width="300">
